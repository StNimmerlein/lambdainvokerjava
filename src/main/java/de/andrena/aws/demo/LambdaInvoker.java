package de.andrena.aws.demo;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.model.ServiceException;

import java.nio.charset.StandardCharsets;

public class LambdaInvoker {

    private static final String FUNCTION_NAME = "Greeter";


    public static void main(String[] args) {
        InvokeRequest request = new InvokeRequest()
                .withFunctionName(LambdaInvoker.FUNCTION_NAME)
                .withPayload("{\"firstName\": \"Marco\", \"lastName\": \"Sieben\", \"age\": 28}");

        InvokeResult result = null;
        try {
            AWSLambda lambda = AWSLambdaClientBuilder.standard()
                    .withCredentials(new ProfileCredentialsProvider())
                    .build();

            result = lambda.invoke(request);

            String resultAsString = new String(result.getPayload().array(), StandardCharsets.UTF_8);

            System.out.println(resultAsString);
        } catch (ServiceException e) {
            System.out.println(e);
        }

        System.out.println(result.getStatusCode());
    }
}
